﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace ExSarine
{
    public partial class Form1 : Form
    {
        private const string INPUT_REGEX = "^@Country: (?<country>.*) Predicted Hits: (?<hits>.*)";
        private const string FILEPATH_CONTINENTS = "CC.txt";
        private const string FILEPATH_OUTPUT = "output.txt";

        private Hashtable countries_to_continents = new Hashtable(); // countries as keys, continents as values

        #region Classes for JSON final output

        public class DataCountry {
            public string Country { get; set; }
            public int Hits { get; set; }
        }

        public class DataJSON {
            public string TopCotinentHits { get; set; } // continent name with most hits
            public List<DataCountry> CountriesHits { get; set; }
        }

        #endregion
        

        public Form1() {
            InitializeComponent();
            InitializeData();
        }

        /// <summary>
        /// Initialize countries_to_continents table from FILEPATH_CONTINENTS file
        /// </summary>
        private void InitializeData() {
            string line;
            StreamReader file =  new StreamReader(FILEPATH_CONTINENTS);
            string cur_continent = null;

            while ((line = file.ReadLine()) != null) {
                if(line.StartsWith("### ")) {
                    cur_continent = line.Substring(4);
                } else {
                    if(!String.IsNullOrEmpty(line))
                        countries_to_continents.Add(line, cur_continent);
                }
            }

            file.Close();
        }

        private void button_browse_Click(object sender, EventArgs e)  {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)  {
                textBox1.Text = openFileDialog1.FileName;
            }
        }

        /// <summary>
        /// return list of hits per country
        /// </summary>
        private List<DataCountry> extractCountriesHitsList() {
            StreamReader reader = File.OpenText(textBox1.Text);
            string line;
            Regex pattern = new Regex(INPUT_REGEX);

            List<DataCountry> countries_hits = new List<DataCountry>();

            while ((line = reader.ReadLine()) != null) {
                // regex to find the country and hits according to agreed format
                Match match = pattern.Match(line);
                if (match.Success) {
                    // create the country-hits object
                    DataCountry data = new DataCountry {
                        Country = match.Groups["country"].Value,
                        Hits = int.Parse(match.Groups["hits"].Value, NumberStyles.AllowThousands)
                    };

                    // adding it to the final list
                    countries_hits.Add(data);
                }

            }

            // sort by hits (decending)
            countries_hits.Sort(delegate (DataCountry dc1, DataCountry dc2) { return dc2.Hits.CompareTo(dc1.Hits); });

            return countries_hits;

        }

        /// <summary>
        /// return the name of the most popular continent (summing hits of countries per continents according to input given)
        /// </summary>
        /// <param name="countries_hits"></param>
        /// <returns></returns>
        private string extractPopularContinent(List<DataCountry> countries_hits) {
            Hashtable hits_per_continent = new Hashtable();
            string most_popular = null;
            int most_popular_hits = 0; // lets assume thers at least one hit somewhere

            // adding hits to hash table
            foreach(DataCountry dc in countries_hits) {
                string continent = (string)countries_to_continents[dc.Country];
                if(hits_per_continent.ContainsKey(continent))
                    hits_per_continent[continent] = (int)hits_per_continent[continent] + dc.Hits;
                else
                    hits_per_continent.Add(continent, dc.Hits);

                // update most popular continent if we surpressed the previous
                int cur_hits = (int)hits_per_continent[continent];
                if (cur_hits > most_popular_hits) {
                    most_popular_hits = cur_hits;
                    most_popular = continent;
                }
            }
            return most_popular;
        }
        

        /// <summary>
        /// generate JSON
        /// </summary>
        private void button1_Click(object sender, EventArgs e) {

            // add countries hits to final object
            DataJSON json_output = new DataJSON {
                CountriesHits = extractCountriesHitsList()
            };

            // add most popular continent to final object
            json_output.TopCotinentHits = extractPopularContinent(json_output.CountriesHits);

            // object to json file
            string json = new JavaScriptSerializer().Serialize(json_output);
            File.WriteAllText(FILEPATH_OUTPUT, json);
            
        }
    }
}
