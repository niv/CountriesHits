﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
/// 
/// Assumptions on The Algorithem output file (which is my input):
/// 1) country names are full names, except UK which can be UK (otherwise can just add the short names to the file, this is not important to the algorithem if the input being consistent)
/// 2) no weird formatting issues (suddenly no whitespace, or line break into multiple lines, etc..)
/// 
/// 
/// 
/// </summary>


namespace ExSarine
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
